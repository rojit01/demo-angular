import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  private url = 'https://jsonplaceholder.typicode.com'
  constructor(
    private http:HttpClient
  ) { }

  getAllTodos(){
    return this.http.get(this.url+'/todos');
  }

  postTodo(data){
    return this.http.post('http://simpliflybackend.bentray.work/api/web/v1/todo',data);
  }
}
