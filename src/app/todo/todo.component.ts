import { Component, OnInit } from '@angular/core';
import { TodoService } from '../service/todo.service';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {
  allTodos;
  showData:boolean = false;
  constructor(
    private tServ:TodoService
  ) { }

  ngOnInit() {
    this.tServ.getAllTodos().subscribe(data=>{
      this.allTodos = data;
      console.log(this.allTodos)
    })
  }

  btnClick(){
    this.showData = !this.showData;
  }

}
