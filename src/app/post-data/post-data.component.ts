import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { TodoService } from '../service/todo.service';

@Component({
  selector: 'app-post-data',
  templateUrl: './post-data.component.html',
  styleUrls: ['./post-data.component.css']
})
export class PostDataComponent implements OnInit {
  formData
  message="";
  constructor(
    private fb:FormBuilder,
    private tServ :TodoService
  ) { }

  ngOnInit() {
    this.formData = this.fb.group({
      title:'',
      completed:'',
      archive:''
    })
  }

  onSubmit(){
    this.tServ.postTodo(this.formData.value).subscribe((data:any)=>{
      this.message = data.data;
      console.log(this.message);

    })
  }

}
