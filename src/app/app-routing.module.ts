import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TodoComponent } from './todo/todo.component';
import { PostDataComponent } from './post-data/post-data.component';


const routes: Routes = [
  {path:'',component : TodoComponent},
  {path:'post-data',component:PostDataComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
